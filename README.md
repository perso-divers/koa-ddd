# Purpose of this project

- Try the DDD with Typescript + Koa + Inversify + Jest.
- Create fictive use cases of an ecommerce website.

# Installation
```bash
npm install
```

# Tests
```bash
npm run test
```

# Start the application
Copy the .env.example
```bash
cp .env.example .env
```
Défine the DB_CONNECTION key.
```bash
DB_CONNECTION='mongodb://mongoDB:27017/kao_test'
```

Build and up the containers
```bash
docker-composer up -d --build
```

# TODO
- Job Gitlab for code style (eslint & prettier)
- Create a login system to learn how to manage in the good way sessions with NodeJS and Hexagonal Archi
