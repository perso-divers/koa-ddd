import { container } from "../../../container";
import { ListProducts } from "../../../core/useCases/product/ListProducts";
import { ProductDto } from "../../../core/domain/product/Product";
import { ShowProduct } from "../../../core/useCases/product/ShowProduct";
import { CreateProduct } from "../../../core/useCases/product/CreateProduct";
import { UpdateProduct } from "../../../core/useCases/product/UpdateProduct";

export async function list(): Promise<{ products: ProductDto[] }> {
    return { products: (await container.resolve(ListProducts).invoke()) as ProductDto[] };
}

export async function show(id: string): Promise<{ product: ProductDto } | Error> {
    const product: ProductDto = await container.resolve(ShowProduct).invoke(id);
    if (product) {
        return { product };
    }
    return ProjectNotFound;
}

export async function create(productDto: ProductDto): Promise<{ product: ProductDto }> {
    return { product: await container.resolve(CreateProduct).invoke(productDto) };
}

export async function update(productDto: ProductDto, id: string): Promise<{ product: ProductDto } | Error> {
    const product: ProductDto = await container.resolve(UpdateProduct).invoke(productDto, id);
    if (product) {
        return { product };
    }
    return ProjectNotFound;
}

type Error = { error: string };

const ProjectNotFound: Error = { error: "invalid product id" };
