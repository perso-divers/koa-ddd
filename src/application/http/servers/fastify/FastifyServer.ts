import "reflect-metadata";
import { ServerInterface } from "../../ServerInterface";
import { injectable } from "inversify";
import Fastify, { FastifyInstance } from "fastify";
import { setRoutes } from "./routes/productsRoutes";

@injectable()
export class FastifyServer implements ServerInterface {
    async startServer(): Promise<void> {
        let server: FastifyInstance = Fastify({ logger: true });
        server = setRoutes(server);
        await server.listen({ port: 3000 });
    }
}
