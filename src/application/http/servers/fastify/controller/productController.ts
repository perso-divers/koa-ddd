import { ProductDto } from "../../../../../core/domain/product/Product";
import { list as commonList, show as commonShow, create as commonCreate, update as commonUpdate } from "../../../common/productController";
import { FastifyReply, FastifyRequest } from "fastify";

export async function list(_request: FastifyRequest, reply: FastifyReply): Promise<string> {
    return reply.send(await commonList());
}

export async function show(request: FastifyRequest, reply: FastifyReply): Promise<void> {
    return reply.send(await commonShow((request.params as { id: string }).id));
}

export async function create(request: FastifyRequest, reply: FastifyReply): Promise<void> {
    return reply.send(await commonCreate(request.body as ProductDto));
}

export async function update(request: FastifyRequest, reply: FastifyReply): Promise<void> {
    return reply.send(await commonUpdate(request.body as ProductDto, (request.params as { id: string }).id));
}
