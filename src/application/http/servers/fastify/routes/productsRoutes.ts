import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify";
import { create, list, show, update } from "../controller/productController";

export const setRoutes = (server: FastifyInstance): FastifyInstance => {
    server.route({
        method: "GET",
        url: "/products",
        async handler(request: FastifyRequest, reply: FastifyReply): Promise<void> {
            await list(request, reply);
        },
    });
    server.route({
        method: "GET",
        url: "/products/:id",
        async handler(request: FastifyRequest, reply: FastifyReply): Promise<void> {
            await show(request, reply);
        },
    });
    server.route({
        method: "PUT",
        url: "/products/:id",
        async handler(request: FastifyRequest, reply: FastifyReply): Promise<void> {
            await update(request, reply);
        },
    });
    server.route({
        method: "POST",
        url: "/products",
        async handler(request: FastifyRequest, reply: FastifyReply): Promise<void> {
            await create(request, reply);
        },
    });
    return server;
};
