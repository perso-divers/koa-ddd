import { Context } from "koa";
import { ProductDto } from "../../../../../core/domain/product/Product";
import { list as commonList, show as commonShow, create as commonCreate, update as commonUpdate } from "../../../common/productController";

export async function list(ctx: Context): Promise<void> {
    ctx.body = await commonList();
}

export async function show(ctx: Context): Promise<void> {
    ctx.body = await commonShow(ctx.params.id);
}

export async function create(ctx: Context): Promise<void> {
    ctx.body = await commonCreate(ctx.body as ProductDto);
}

export async function update(ctx: Context): Promise<void> {
    ctx.body = await commonUpdate(ctx.body as ProductDto, ctx.params.id);
}
