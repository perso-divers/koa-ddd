import "reflect-metadata";
import { ServerInterface } from "../../ServerInterface";
import { injectable } from "inversify";
import { NestFactory } from "@nestjs/core";
import { ProductModule } from "./controller/product.module";
import { INestApplication } from "@nestjs/common";

@injectable()
export class NestServer implements ServerInterface {
    async startServer(): Promise<void> {
        const app: INestApplication = await NestFactory.create(ProductModule);
        await app.listen(3000);
    }
}
