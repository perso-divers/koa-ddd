import { Body, Controller, Get, Param, Post, Put } from "@nestjs/common";
import { ProductDto } from "../../../../../core/domain/product/Product";
import { list as commonList, show as commonShow, create as commonCreate, update as commonUpdate } from "../../../common/productController";

@Controller()
export class ProductController {
    @Get("/products")
    async list(): Promise<{ products: ProductDto[] }> {
        return await commonList();
    }

    @Get("/products/:id")
    async show(@Param() params: { id: string }): Promise<{ product: ProductDto } | { error: string }> {
        return await commonShow(params.id);
    }

    @Post("/products")
    async create(@Body() body: ProductDto): Promise<{ product: ProductDto }> {
        return await commonCreate(body as ProductDto);
    }

    @Put("/products/:id")
    async update(@Param() params: { id: string }, @Body() body: ProductDto): Promise<{ product: ProductDto } | { error: string }> {
        return await commonUpdate(body as ProductDto, params.id);
    }
}
