import { Product, ProductDto } from "../../domain/product/Product";
import "reflect-metadata";
import { injectable } from "inversify";
import { AbstractProduct } from "./AbstractProduct";

@injectable()
export class ShowProduct extends AbstractProduct {
    public async invoke(idProduct: string): Promise<ProductDto | null> {
        const product: Product | null = await this.repository.show(idProduct);
        return product ? product.toDto() : null;
    }
}
