import "reflect-metadata";
import { injectable } from "inversify";
import { Product, ProductDto } from "../../../../core/domain/product/Product";
import { ProductRepositoryInterface } from "../../../../core/domain/product/ProductRepositoryInterface";

const products: Product[] = [new Product({ id: "id1", reference: "ref1", name: "iphone", price: 34, updated_at: new Date(), created_at: new Date() }), new Product({ id: "id2", reference: "ref2", name: "tv", price: 56, updated_at: new Date(), created_at: new Date() })];

@injectable()
export class ProductRepository implements ProductRepositoryInterface {
    async list(): Promise<Product[]> {
        return products;
    }
    async show(id: string): Promise<Product | null> {
        return (
            products.filter((product) => {
                return product.id === id;
            })[0] ?? null
        );
    }
    async store(productDto: ProductDto): Promise<Product> {
        const product = new Product(productDto);
        products.push(product);
        return product;
    }
    async update(productDto: ProductDto, idProduct: string): Promise<Product> {
        const index: number = products.findIndex((obj) => obj.id === idProduct);
        const product = new Product(productDto);
        products[index] = product;
        return product;
    }
}
